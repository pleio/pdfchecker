# pdfchecker-cms

## How to Use

`yarn dev` will start up your application and reload on any changes.

### Docker

If you have docker and docker-compose installed, you can run `docker-compose up`

To build the docker image, run `docker build -t my-tag .`

Ensure you are passing all needed environment variables when starting up your
container via `--env-file` or setting them with your deployment.

The 3 typical env vars will be `DATABASE_URI`, `PAYLOAD_SECRET`, and
`PAYLOAD_CONFIG_PATH`

`docker run --env-file .env -p 3000:3000 my-tag`

## Import data

You can use `import-rules` script to import rules from a JSON file. For messages (nl, en, de and technical) the JSON file  
should have the following structure:

```json
{
    "WCAG2.2": {
        "1": {
            "1.1.1": {
                "SUMMARY": "Lorem ipsum",
                "DESCRIPTION": "Lorem ipsum"
            }
            // ...
        }
        // ...
    },
    "ISO 14289-1:2014": {
        "1": {
            "1.1": {
                "SUMMARY": "Lorem ipsum",
                "DESCRIPTION": "Lorem ipsum"
            }
            // ...
        }
        // ...
    }
}
```

For meta data, the JSON file should have the following structure:

```json
{
    "WCAG2.2": {
        "1": {
            "1.1.1": {
                "STATUS": "warning",
                "CATEGORY": "structure",
                "RATIONALE": "Lorem ipsum"
            }
            // ...
        }
        // ...
    },
    "ISO 14289-1:2014": {
        "1": {
            "1.1": {
                "STATUS": "warning",
                "CATEGORY": "structure",
                "RATIONALE": "Lorem ipsum"
            }
            // ...
        }
        // ...
    }
}
```

Run the command as follows

```bash
yarn manage:dev -- import-rules --type=messages --locale=nl --filePath=errorMessages_nl.json --dry-run --debug
```

-   `filepath` is required and should be the path to the JSON file. The path is relative to the `cms` directory
-   `type` is required and should be either `messages` or `technical` or `meta`
-   `locale` is required for `type=messages`, can be `nl`, `de` or `en`
-   `dry-run` is optional and defaults to `false`. If set to `true` (or `1` or
    `'true'`), it will not update the database, but only logs the changes it would
    make
-   `debug` is optional and defaults to `false`. If set to `true` (or `1` or
    `'true'`), it will log more information about the data that it is being
    processed

## Export data

You can use `export-rules` script to export rules to a JSON file. The output files will have the same structure as described above.

Run the command as follows

```bash
yarn manage:dev -- export-rules --type=messages --locale=nl
```

-   `type` is required and should be either `messages` or `technical` or `meta`
-   `locale` is required for `type=messages`, can be `nl`, `de` or `en`
