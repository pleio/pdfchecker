import { Endpoint } from 'payload/config'
import { PaginatedDocs } from 'payload/database'
import { Iso, Wcag } from 'payload/generated-types'
import { PayloadRequest } from 'payload/types'
import { z } from 'zod'

const filterSchema = z.object({
    specification: z.string(),
    clauses: z.array(
        z.object({
            clause: z.string(),
            testNumbers: z.array(z.union([z.string(), z.number()])),
        })
    ),
})

const filterListSchema = z.array(filterSchema)

export type FilterListSchema = z.infer<typeof filterListSchema>
export type FilterSchema = z.infer<typeof filterSchema>

export const getAllRules: Endpoint = {
    path: '/rules',
    method: 'get',
    handler: async (req: PayloadRequest, res) => {
        const { payload, query } = req
        const locale = typeof query.locale === 'string' ? query.locale : 'nl'
        const filter = typeof query.filter === 'string' ? JSON.parse(query.filter) : []

        const filterRules = async (
            colllectionSlug: 'iso' | 'wcag',
            filter: FilterSchema['clauses']
        ) => {
            // Get all rules for a collection.
            // If there is a filter, only get certain clauses
            const rules = (await payload.find({
                collection: colllectionSlug,
                pagination: false,
                locale,
                ...(filter.length > 0
                    ? {
                          where: {
                              clause: { in: filter?.map((c) => c.clause) },
                          },
                      }
                    : {}),
            })) as unknown as PaginatedDocs<Iso | Wcag>

            // If there is no filter, return all rules
            if (filter.length === 0) {
                return rules.docs
            }

            // If there is a filter, only return the testnumbers in clauses that are in the filter
            return rules.docs.map((rule) => ({
                ...rule,
                testNumbers: rule.testNumbers.filter((testNumber) => {
                    const selectedClause = filter.find((c) => c.clause === rule.clause)
                    return selectedClause
                        ? selectedClause.testNumbers.includes(testNumber.number)
                        : false
                }),
            }))
        }

        let parsedFilter

        try {
            parsedFilter = filterListSchema.parse(filter)
        } catch (e) {
            // No correct filter, so fetching all rules
            /* TODO: report error at least somewhere */
            const rules = [...(await filterRules('iso', [])), ...(await filterRules('wcag', []))]
            return res.status(200).send(rules)
        }

        const isoRules = await filterRules(
            'iso',
            parsedFilter.find((i) => i.specification === 'ISO 14289-1:2014')?.clauses || []
        )

        const wcagRules = await filterRules(
            'wcag',
            parsedFilter.find((i) => i.specification === 'WCAG2.2')?.clauses || []
        )

        const rules = [...isoRules, ...wcagRules]

        return res.status(200).send(rules)
    },
}
