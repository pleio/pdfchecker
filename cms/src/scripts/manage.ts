import dotenv from 'dotenv'
import parseArgs from 'minimist'
import path from 'path'
import payload from 'payload'
import { importRules } from './importRules/importRules'
import { exportRules } from './exportRules/exportRules'

dotenv.config({
    path: path.resolve(__dirname, '../../.env'),
})

const COMMANDS = [
    {
        name: 'import-rules',
        handler: importRules,
    },
    {
        name: 'export-rules',
        handler: exportRules,
    },
]

const { PAYLOAD_SECRET } = process.env

const args = parseArgs(process.argv.slice(2), { boolean: true })
// _ contains all arguments that do not have an option up front. First one will be considered as a manage command
const { _: commands, ...options } = args

const command = COMMANDS.find((command) => command.name === commands[0])

if (!command) {
    console.error(
        `Invalid command "${command || ''}".  Must be one of: ${COMMANDS.map(
            (command) => command.name
        ).join(', ')}`
    )
    process.exit(1)
}

payload
    .init({
        secret: PAYLOAD_SECRET,
        local: true,
    })
    .then(command.handler(options as any))
    .then(() => process.exit())
