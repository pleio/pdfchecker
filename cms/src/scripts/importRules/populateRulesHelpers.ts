import { MessageSchema, MetaSchema } from './populateRulesSchemas'
import { Iso, Wcag } from 'payload/generated-types'

type PayloadTestNumbers = Wcag['testNumbers'] | Iso['testNumbers']

export const transformTestNumbersMetaInputToPayload = (
    testNumbers: Record<string, MetaSchema>
): PayloadTestNumbers =>
    Object.entries(testNumbers).map(([number, metas]) => ({
        number: parseInt(number, 10),
        rationale: metas.RATIONALE,
        status: metas.STATUS as 'warning' | 'error' | 'ignore' | 'critical' | 'serious',
        category: metas.CATEGORY as 'structure' | 'code' | 'metadata' | 'alt-text',
    }))

export const transformTestNumbersMessageInputToPayload = (
    testNumbers: Record<string, MessageSchema>,
    key: 'message' | 'technical'
): PayloadTestNumbers =>
    Object.entries(testNumbers).map(([number, messages]) => ({
        number: parseInt(number, 10),
        [key]: {
            summary: messages.SUMMARY,
            description: messages.DESCRIPTION,
        },
    }))

export const getUpdatedMetaTestNumbers = (
    prev: PayloadTestNumbers,
    next: Record<string, MetaSchema>
) => {
    const remainingNext = { ...next }
    let updatedTestNumbers: PayloadTestNumbers = prev.map((testNumber) => {
        const metas = next[testNumber.number.toString()]
        if (metas) {
            delete remainingNext[testNumber.number.toString()]
        }
        return {
            ...testNumber,
            rationale: metas?.RATIONALE || testNumber.rationale,
            status: metas?.STATUS || testNumber.status,
            category: metas?.CATEGORY || testNumber.category,
        }
    })

    updatedTestNumbers = [
        ...updatedTestNumbers,
        ...transformTestNumbersMetaInputToPayload(remainingNext),
    ]

    return updatedTestNumbers
}

export const getUpdatedMessageTestNumbers = (
    key: 'message' | 'technical',
    prev: PayloadTestNumbers,
    next: Record<string, MessageSchema>
) => {
    const remainingNext = { ...next }
    let updatedTestNumbers: PayloadTestNumbers = prev.map((testNumber) => {
        const messages = next[testNumber.number.toString()]
        if (messages) {
            delete remainingNext[testNumber.number.toString()]
        }
        return {
            ...testNumber,
            [key]: {
                summary: messages?.SUMMARY || testNumber[key].summary,
                description: messages?.DESCRIPTION || testNumber[key].description,
            },
        }
    })

    updatedTestNumbers = [
        ...updatedTestNumbers,
        ...transformTestNumbersMessageInputToPayload(remainingNext, key),
    ]

    return updatedTestNumbers
}
