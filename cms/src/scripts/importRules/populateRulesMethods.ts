import payload from 'payload'
import { MessageSchema, MessagesSchema, MetaSchema, MetasSchema } from './populateRulesSchemas'
import {
    getUpdatedMessageTestNumbers,
    getUpdatedMetaTestNumbers,
    transformTestNumbersMessageInputToPayload,
    transformTestNumbersMetaInputToPayload,
} from './populateRulesHelpers'
import { PaginatedDocs } from 'payload/database'
import { Iso, Wcag } from 'payload/generated-types'

const populateForCollection = async (
    data:
        | Record<string, Record<string, MessageSchema>>
        | Record<string, Record<string, MetaSchema>>,
    type: 'message' | 'technical' | 'meta',
    collectionSlug: 'iso' | 'wcag',
    locale: string,
    dryRun: boolean
) => {
    for (const [clause, numbers] of Object.entries(data)) {
        const existingClause = (await payload.find({
            collection: collectionSlug,
            locale: locale,
            where: {
                clause: { equals: clause },
            },
        })) as unknown as PaginatedDocs<Iso | Wcag>

        if (existingClause.totalDocs > 1) {
            payload.logger.warn(`Multiple entries found for ${collectionSlug} ${clause}`)
        }

        if (existingClause.totalDocs === 0) {
            const testNumbers =
                type === 'meta'
                    ? transformTestNumbersMetaInputToPayload(numbers)
                    : transformTestNumbersMessageInputToPayload(numbers, type)

            payload.logger.info(
                dryRun
                    ? `Will create ${collectionSlug} ${clause}`
                    : `Creating ${collectionSlug} ${clause}`
            )

            if (!dryRun) {
                await payload.create({
                    collection: collectionSlug,
                    locale: locale,
                    data: {
                        clause,
                        testNumbers,
                    },
                })
            }
        }

        if (existingClause.totalDocs === 1) {
            const testNumbers =
                type === 'meta'
                    ? getUpdatedMetaTestNumbers(existingClause.docs[0].testNumbers, numbers)
                    : getUpdatedMessageTestNumbers(
                          type,
                          existingClause.docs[0].testNumbers,
                          numbers
                      )

            payload.logger.info(
                dryRun
                    ? `Will update ${collectionSlug} ${clause}`
                    : `Updating ${collectionSlug} ${clause}`
            )

            if (!dryRun) {
                await payload.update({
                    collection: collectionSlug,
                    locale: locale,
                    id: existingClause.docs[0].id,
                    data: { testNumbers },
                })
            }
        }
    }
}

export const populateMeta = async (rules: MetasSchema, dryRun: boolean) => {
    payload.logger.info(`Populating metas`)

    await populateForCollection(rules['ISO 14289-1:2014'], 'meta', 'iso', 'nl', dryRun)
    await populateForCollection(rules['WCAG2.2'], 'meta', 'wcag', 'nl', dryRun)
}

export const populateMessages = async (rules: MessagesSchema, dryRun: boolean, locale: string) => {
    payload.logger.info(`Populating messages for locale ${locale}`)

    await populateForCollection(rules['ISO 14289-1:2014'], 'message', 'iso', locale, dryRun)
    await populateForCollection(rules['WCAG2.2'], 'message', 'wcag', locale, dryRun)
}

export const populateTechnicalMessages = async (rules: MessagesSchema, dryRun: boolean) => {
    payload.logger.info('Populating technical messages')

    await populateForCollection(rules['ISO 14289-1:2014'], 'technical', 'iso', 'nl', dryRun)
    await populateForCollection(rules['WCAG2.2'], 'technical', 'wcag', 'nl', dryRun)
}
