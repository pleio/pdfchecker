import payload from 'payload'
import { z } from 'zod'

// Schema for the meta fields
const metaSchema = z.object({
    STATUS: z.union([
        z.literal('warning'),
        z.literal('error'),
        z.literal('ignore'),
        z.literal('critical'),
        z.literal('serious'),
    ]),
    CATEGORY: z.union([
        z.literal('structure'),
        z.literal('code'),
        z.literal('metadata'),
        z.literal('alt-text'),
    ]),
    RATIONALE: z.string(),
})

export type MetaSchema = z.infer<typeof metaSchema>

// Main schema for the entire structure
const metasSchema = z.object({
    'WCAG2.2': z.record(z.record(metaSchema)),
    'ISO 14289-1:2014': z.record(z.record(metaSchema)),
})

export type MetasSchema = z.infer<typeof metasSchema>

export const metasAreValid = (metas: unknown, logError: boolean): metas is MetasSchema => {
    try {
        metasSchema.parse(metas)
        return true
    } catch (error) {
        if (logError) payload.logger.error(error)
        return false
    }
}

// Schema for the message fields
const messageSchema = z.object({
    SUMMARY: z.string(),
    DESCRIPTION: z.string(),
})

export type MessageSchema = z.infer<typeof messageSchema>

// Main schema for the entire structure
const messagesSchema = z.object({
    'WCAG2.2': z.record(z.record(messageSchema)),
    'ISO 14289-1:2014': z.record(z.record(messageSchema)),
})

export type MessagesSchema = z.infer<typeof messagesSchema>

export const messagesAreValid = (
    messages: unknown,
    logError: boolean
): messages is MessagesSchema => {
    try {
        messagesSchema.parse(messages)
        return true
    } catch (error) {
        if (logError) payload.logger.error(error)
        return false
    }
}
