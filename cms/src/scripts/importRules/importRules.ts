import payload from 'payload'
import { populateMessages, populateMeta, populateTechnicalMessages } from './populateRulesMethods'
import { messagesAreValid, metasAreValid } from './populateRulesSchemas'
import path from 'path'

interface ImportRulesArguments {
    type?: 'messages' | 'technical' | 'meta'
    locale?: 'en' | 'de' | 'nl'
    filePath?: string
    'dry-run'?: string | number
    debug?: string | number
}

const argToBoolean = (arg: boolean | string | number | undefined): boolean =>
    arg === true || arg === 'true' || arg === '1' || arg === 1

/**
 * Populate rules based on a json file with a predefined structure.
 */
export const importRules =
    ({ type, locale, 'dry-run': dryRunArg, debug: debugArg, filePath }: ImportRulesArguments) =>
    async () => {
        if (!filePath) {
            payload.logger.error('File path is required')
            return
        }

        if (!type) {
            payload.logger.error('Type is required')
            return
        }
        if (['messages', 'technical', 'meta'].indexOf(type) === -1) {
            payload.logger.error(
                'Invalid type, only "messages", "technical" and "meta" are allowed'
            )
            return
        }
        if (type === 'messages' && !locale) {
            payload.logger.error('Locale is required for messages')
            return
        }
        if (type === 'messages' && ['en', 'de', 'nl'].indexOf(locale) === -1) {
            payload.logger.error('Invalid locale, only "en", "de", "nl" are allowed')
            return
        }

        const dryRun = argToBoolean(dryRunArg)
        const debug = argToBoolean(debugArg)
        const absoluteFilePath = path.join(process.cwd(), filePath)

        if (dryRun) {
            payload.logger.warn('Dry run enabled. No changes will be made')
        }

        const rules = require(absoluteFilePath)

        if (type === 'messages') {
            if (!messagesAreValid(rules, debug)) {
                payload.logger.error('Invalid messages structure')
                return
            }
            await populateMessages(rules, dryRun, locale)
        }

        if (type === 'technical') {
            if (!messagesAreValid(rules, debug)) {
                payload.logger.error('Invalid technical messages structure')
                return
            }
            await populateTechnicalMessages(rules, dryRun)
        }

        if (type === 'meta') {
            if (!metasAreValid(rules, debug)) {
                payload.logger.error('Invalid meta structure')
                return
            }
            await populateMeta(rules, dryRun)
        }
    }
