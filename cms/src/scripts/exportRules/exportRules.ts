import payload from 'payload'
import { MessageSchema, MetaSchema } from '../importRules/populateRulesSchemas'
import { Iso, Wcag } from 'payload/generated-types'
import { writeFile } from 'node:fs/promises'
import { PaginatedDocs } from 'payload/database'
import { TypeWithID } from 'payload/types'

interface ExportRulesArguments {
    type?: 'messages' | 'technical' | 'meta'
    locale?: 'en' | 'de' | 'nl'
}

/**
 * Export rules based to a json file.
 */
export const exportRules =
    ({ type, locale }: ExportRulesArguments) =>
    async () => {
        if (!type) {
            payload.logger.error('Type is required')
            return
        }
        if (['messages', 'technical', 'meta'].indexOf(type) === -1) {
            payload.logger.error(
                'Invalid type, only "messages", "technical" and "meta" are allowed'
            )
            return
        }
        if (type === 'messages' && !locale) {
            payload.logger.error('Locale is required for messages')
            return
        }
        if (type === 'messages' && ['en', 'de', 'nl'].indexOf(locale) === -1) {
            payload.logger.error('Invalid locale, only "en", "de", "nl" are allowed')
            return
        }

        switch (type) {
            case 'messages':
                await exportRulesFor('message', locale)
                break
            case 'technical':
                await exportRulesFor('technical')
                break
            case 'meta':
                await exportRulesFor('meta')
                break
        }
    }

const exportRulesFor = async (type: 'message' | 'technical' | 'meta', locale?: string) => {
    const isoItems = (await payload.find({
        collection: 'iso',
        locale,
        pagination: false,
    })) as unknown as PaginatedDocs<Iso>

    const wcagItems = (await payload.find({
        collection: 'wcag',
        locale,
        pagination: false,
    })) as unknown as PaginatedDocs<Wcag>

    const outputJson = {
        'ISO 14289-1:2014': isoItems.docs?.reduce(
            (acc, item) => {
                acc[item.clause] =
                    type === 'meta'
                        ? transformTestNumbersMetaPayloadToOutput(item.testNumbers)
                        : transformTestNumbersMessagePayloadToOutput(item.testNumbers, type)
                return acc
            },
            {} as Record<string, Record<string, MessageSchema | MetaSchema>>
        ),
        'WCAG2.2': wcagItems.docs?.reduce(
            (acc, item) => {
                acc[item.clause] =
                    type === 'meta'
                        ? transformTestNumbersMetaPayloadToOutput(item.testNumbers)
                        : transformTestNumbersMessagePayloadToOutput(item.testNumbers, type)
                return acc
            },
            {} as Record<string, Record<string, MessageSchema | MetaSchema>>
        ),
    }

    const filePath = locale ? `error-${type}-${locale}.json` : `error-${type}.json`

    try {
        await writeFile(filePath, JSON.stringify(outputJson, null, 2), 'utf8')
    } catch (error) {
        payload.logger.error(`Error writing file: ${error}`)
    }

    payload.logger.info(`Exported to ${filePath}`)
}

type PayloadTestNumbers = Wcag['testNumbers'] | Iso['testNumbers']

export const transformTestNumbersMetaPayloadToOutput = (
    testNumbers: PayloadTestNumbers
): Record<string, MetaSchema> =>
    testNumbers.reduce(
        (acc, testNumber) => {
            acc[testNumber.number.toString()] = {
                STATUS: testNumber.status,
                CATEGORY: testNumber.category,
                RATIONALE: testNumber.rationale,
            }
            return acc
        },
        {} as Record<string, MetaSchema>
    )

export const transformTestNumbersMessagePayloadToOutput = (
    testNumbers: PayloadTestNumbers,
    key: 'message' | 'technical'
): Record<string, MessageSchema> =>
    testNumbers.reduce(
        (acc, testNumber) => {
            acc[testNumber.number.toString()] = {
                SUMMARY: testNumber[key].summary,
                DESCRIPTION: testNumber[key].description,
            }
            return acc
        },
        {} as Record<string, MessageSchema>
    )
