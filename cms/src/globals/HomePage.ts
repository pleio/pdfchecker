import { RowLabelArgs } from 'payload/dist/admin/components/forms/RowLabel/types'
import { GlobalConfig } from 'payload/types'

const HomePage: GlobalConfig = {
    slug: 'home-page',
    label: 'Home',
    access: {
        read: () => true,
    },
    admin: {
        group: 'Pages',
    },
    fields: [
        {
            name: 'title',
            label: 'Title',
            type: 'text',
            localized: true,
        },
        {
            name: 'intro',
            label: 'Introduction',
            type: 'textarea',
            localized: true,
        },
        { name: 'alert', label: 'Alert', type: 'text', localized: true },
        {
            type: 'array',
            name: 'content',
            label: 'Content',
            admin: {
                components: {
                    RowLabel: ({ index }: RowLabelArgs) => `Column ${index}`,
                },
            },
            fields: [
                {
                    name: 'richText',
                    label: 'Column',
                    type: 'richText',
                    localized: true,
                },
            ],
        },
    ],
}

export default HomePage
