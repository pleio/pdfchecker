import { GlobalConfig } from 'payload/types'

const AboutPage: GlobalConfig = {
    slug: 'about-page',
    label: 'About',
    access: {
        read: () => true,
    },
    admin: {
        group: 'Pages',
    },
    fields: [
        { name: 'title', label: 'Title', type: 'text', localized: true },
        {
            name: 'content',
            label: 'Content',
            type: 'richText',
            localized: true,
        },
        {
            name: 'versionInfo',
            label: 'Version Info',
            type: 'group',
            fields: [
                {
                    type: 'row',
                    fields: [
                        {
                            name: 'backend',
                            label: 'Backend',
                            type: 'text',
                            localized: true,
                        },
                        {
                            name: 'frontend',
                            label: 'Frontend',
                            type: 'text',
                            localized: true,
                        },
                    ],
                },
            ],
        },
    ],
}

export default AboutPage
