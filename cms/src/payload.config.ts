import { webpackBundler } from '@payloadcms/bundler-webpack'
import { slateEditor } from '@payloadcms/richtext-slate'
import path from 'path'
import { buildConfig } from 'payload/config'
import ISO from './collections/ISO'
import Users from './collections/Users'
import WCAG from './collections/WCAG'
import { getAllRules } from './endpoints/getAllRules'
import { postgresAdapter } from '@payloadcms/db-postgres'
import HomePage from './globals/HomePage'
import AboutPage from './globals/AboutPage'

export default buildConfig({
    admin: {
        bundler: webpackBundler(),
        meta: {
            titleSuffix: '- PDFchecker CMS',
        },
        user: Users.slug,
    },
    routes: {
        api: '/cms/api',
        admin: '/cms/admin',
        graphQL: '/cms/graphql',
        graphQLPlayground: '/cms/graphql-playground',
    },
    cors: [process.env.PAYLOAD_PUBLIC_APP_URL || 'http://localhost:3000'],
    collections: [Users, ISO, WCAG],
    globals: [HomePage, AboutPage],
    localization: {
        locales: ['nl', 'en', 'de'],
        defaultLocale: 'nl',
        fallback: true,
    },
    typescript: {
        outputFile: path.resolve(__dirname, 'payload-types.ts'),
    },
    graphQL: {
        schemaOutputFile: path.resolve(__dirname, 'generated-schema.graphql'),
    },
    editor: slateEditor({
        admin: {
            link: {
                fields: ({ defaultFields }) =>
                    defaultFields.filter(
                        (field) =>
                            'name' in field && (field.name === 'text' || field.name === 'url')
                    ),
            },
            leaves: ['bold'],
            elements: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'link'],
        },
    }),
    db: postgresAdapter({
        pool: {
            connectionString: process.env.DATABASE_URI,
        },
    }),
    telemetry: false,
    debug: Number(process.env.DEBUG) === 1,
    endpoints: [getAllRules],
})
