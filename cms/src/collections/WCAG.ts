import ruleCollection from './ruleCollection'

const WCAG = ruleCollection({ slug: 'wcag', label: 'WCAG', specification: 'WCAG2.2' })

export default WCAG
