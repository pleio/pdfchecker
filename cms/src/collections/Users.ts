import { CollectionConfig } from 'payload/types'

const Users: CollectionConfig = {
    slug: 'users',
    labels: {
        singular: { en: 'User', nl: 'Gebruiker' },
        plural: { en: 'Users', nl: 'Gebruikers' },
    },
    auth: {},
    admin: {
        useAsTitle: 'email',
        group: {
            en: 'Collections',
            nl: 'Collecties',
        },
        defaultColumns: ['email', 'roles'],
        disableDuplicate: true,
        enableRichTextLink: false,
        enableRichTextRelationship: false,
    },
    fields: [],
}

export default Users
