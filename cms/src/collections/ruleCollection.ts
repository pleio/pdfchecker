import { CollectionConfig } from 'payload/types'
import { merge } from 'lodash'

const statusOptions = [
    {
        label: 'Warning',
        value: 'warning',
    },
    {
        label: 'Error',
        value: 'error',
    },
    {
        label: 'Ignore',
        value: 'ignore',
    },
    {
        label: 'Critical',
        value: 'critical',
    },
    {
        label: 'Serious',
        value: 'serious',
    },
]

const categoryOptions = [
    {
        label: 'Structure',
        value: 'structure',
    },
    {
        label: 'Code',
        value: 'code',
    },
    {
        label: 'Metadata',
        value: 'metadata',
    },
    {
        label: 'Alt-text',
        value: 'alt-text',
    },
]

const validateUniqueTestNumbers = (value) => {
    const numbers = value?.map((item) => item.number) ?? []
    const uniqueNumbers = [...new Set(numbers)]

    if (numbers.length !== uniqueNumbers.length) {
        return 'Test numbers must be unique'
    }
    return true
}

interface RuleCollectionConfig {
    slug: string
    label: string
    specification: string
}

const ruleCollection = (
    { slug, label, specification }: RuleCollectionConfig,
    overrides: Partial<CollectionConfig> = {}
): CollectionConfig =>
    merge<CollectionConfig, Partial<CollectionConfig>>(
        {
            slug,
            labels: {
                singular: { en: label, nl: label },
                plural: { en: label, nl: label },
            },
            admin: {
                group: {
                    en: 'Collections',
                    nl: 'Collecties',
                },
                useAsTitle: 'clause',
            },
            access: {
                read: () => true,
            },
            hooks: {
                afterRead: [
                    ({ doc }) => ({
                        ...doc,
                        specification,
                    }),
                ],
            },
            fields: [
                {
                    type: 'text',
                    name: 'clause',
                    required: true,
                    unique: true,
                },
                {
                    type: 'array',
                    name: 'testNumbers',
                    admin: {
                        components: {
                            RowLabel: ({ data }) => data.number,
                        },
                    },
                    validate: validateUniqueTestNumbers,
                    fields: [
                        {
                            type: 'row',
                            fields: [
                                {
                                    type: 'number',
                                    name: 'number',
                                    required: true,
                                    admin: {
                                        width: '20%',
                                    },
                                },
                                {
                                    type: 'select',
                                    name: 'status',
                                    admin: {
                                        width: '40%',
                                    },
                                    defaultValue: 'warning',
                                    options: statusOptions,
                                },
                                {
                                    type: 'select',
                                    name: 'category',
                                    admin: {
                                        width: '40%',
                                    },
                                    defaultValue: 'code',
                                    options: categoryOptions,
                                },
                            ],
                        },
                        {
                            type: 'text',
                            name: 'rationale',
                        },
                        {
                            type: 'group',
                            name: 'message',
                            fields: [
                                {
                                    type: 'text',
                                    name: 'summary',
                                    localized: true,
                                },
                                {
                                    type: 'text',
                                    name: 'description',
                                    localized: true,
                                },
                            ],
                        },
                        {
                            type: 'group',
                            name: 'technical',
                            label: 'English technical details',
                            fields: [
                                {
                                    type: 'text',
                                    name: 'summary',
                                },
                                {
                                    type: 'text',
                                    name: 'description',
                                },
                            ],
                        },
                    ],
                },
            ],
        },
        overrides
    )

export default ruleCollection
