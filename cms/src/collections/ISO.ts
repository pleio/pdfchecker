import ruleCollection from './ruleCollection'

const ISO = ruleCollection({ slug: 'iso', label: 'ISO', specification: 'ISO 14289-1:2014' })

export default ISO
