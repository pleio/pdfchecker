import { MigrateUpArgs, MigrateDownArgs } from '@payloadcms/db-postgres'
import { sql } from 'drizzle-orm'

export async function up({ payload }: MigrateUpArgs): Promise<void> {
await payload.db.drizzle.execute(sql`

DO $$ BEGIN
 CREATE TYPE "_locales" AS ENUM('nl', 'en', 'de');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
 CREATE TYPE "enum_iso_test_numbers_status" AS ENUM('warning', 'error', 'ignore', 'critical', 'serious');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
 CREATE TYPE "enum_iso_test_numbers_category" AS ENUM('structure', 'code', 'metadata', 'alt-text');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
 CREATE TYPE "enum_wcag_test_numbers_status" AS ENUM('warning', 'error', 'ignore', 'critical', 'serious');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
 CREATE TYPE "enum_wcag_test_numbers_category" AS ENUM('structure', 'code', 'metadata', 'alt-text');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

CREATE TABLE IF NOT EXISTS "users" (
	"id" serial PRIMARY KEY NOT NULL,
	"updated_at" timestamp(3) with time zone DEFAULT now() NOT NULL,
	"created_at" timestamp(3) with time zone DEFAULT now() NOT NULL,
	"email" varchar NOT NULL,
	"reset_password_token" varchar,
	"reset_password_expiration" timestamp(3) with time zone,
	"salt" varchar,
	"hash" varchar,
	"login_attempts" numeric,
	"lock_until" timestamp(3) with time zone
);

CREATE TABLE IF NOT EXISTS "iso_test_numbers" (
	"_order" integer NOT NULL,
	"_parent_id" integer NOT NULL,
	"id" varchar PRIMARY KEY NOT NULL,
	"number" numeric NOT NULL,
	"status" "enum_iso_test_numbers_status",
	"category" "enum_iso_test_numbers_category",
	"rationale" varchar,
	"technical_summary" varchar,
	"technical_description" varchar
);

CREATE TABLE IF NOT EXISTS "iso_test_numbers_locales" (
	"message_summary" varchar,
	"message_description" varchar,
	"id" serial PRIMARY KEY NOT NULL,
	"_locale" "_locales" NOT NULL,
	"_parent_id" varchar NOT NULL,
	CONSTRAINT "iso_test_numbers_locales_locale_parent_id_unique" UNIQUE("_locale","_parent_id")
);

CREATE TABLE IF NOT EXISTS "iso" (
	"id" serial PRIMARY KEY NOT NULL,
	"clause" varchar NOT NULL,
	"updated_at" timestamp(3) with time zone DEFAULT now() NOT NULL,
	"created_at" timestamp(3) with time zone DEFAULT now() NOT NULL
);

CREATE TABLE IF NOT EXISTS "wcag_test_numbers" (
	"_order" integer NOT NULL,
	"_parent_id" integer NOT NULL,
	"id" varchar PRIMARY KEY NOT NULL,
	"number" numeric NOT NULL,
	"status" "enum_wcag_test_numbers_status",
	"category" "enum_wcag_test_numbers_category",
	"rationale" varchar,
	"technical_summary" varchar,
	"technical_description" varchar
);

CREATE TABLE IF NOT EXISTS "wcag_test_numbers_locales" (
	"message_summary" varchar,
	"message_description" varchar,
	"id" serial PRIMARY KEY NOT NULL,
	"_locale" "_locales" NOT NULL,
	"_parent_id" varchar NOT NULL,
	CONSTRAINT "wcag_test_numbers_locales_locale_parent_id_unique" UNIQUE("_locale","_parent_id")
);

CREATE TABLE IF NOT EXISTS "wcag" (
	"id" serial PRIMARY KEY NOT NULL,
	"clause" varchar NOT NULL,
	"updated_at" timestamp(3) with time zone DEFAULT now() NOT NULL,
	"created_at" timestamp(3) with time zone DEFAULT now() NOT NULL
);

CREATE TABLE IF NOT EXISTS "payload_preferences" (
	"id" serial PRIMARY KEY NOT NULL,
	"key" varchar,
	"value" jsonb,
	"updated_at" timestamp(3) with time zone DEFAULT now() NOT NULL,
	"created_at" timestamp(3) with time zone DEFAULT now() NOT NULL
);

CREATE TABLE IF NOT EXISTS "payload_preferences_rels" (
	"id" serial PRIMARY KEY NOT NULL,
	"order" integer,
	"parent_id" integer NOT NULL,
	"path" varchar NOT NULL,
	"users_id" integer
);

CREATE TABLE IF NOT EXISTS "payload_migrations" (
	"id" serial PRIMARY KEY NOT NULL,
	"name" varchar,
	"batch" numeric,
	"updated_at" timestamp(3) with time zone DEFAULT now() NOT NULL,
	"created_at" timestamp(3) with time zone DEFAULT now() NOT NULL
);

CREATE INDEX IF NOT EXISTS "users_created_at_idx" ON "users" ("created_at");
CREATE UNIQUE INDEX IF NOT EXISTS "users_email_idx" ON "users" ("email");
CREATE INDEX IF NOT EXISTS "iso_test_numbers_order_idx" ON "iso_test_numbers" ("_order");
CREATE INDEX IF NOT EXISTS "iso_test_numbers_parent_id_idx" ON "iso_test_numbers" ("_parent_id");
CREATE UNIQUE INDEX IF NOT EXISTS "iso_clause_idx" ON "iso" ("clause");
CREATE INDEX IF NOT EXISTS "iso_created_at_idx" ON "iso" ("created_at");
CREATE INDEX IF NOT EXISTS "wcag_test_numbers_order_idx" ON "wcag_test_numbers" ("_order");
CREATE INDEX IF NOT EXISTS "wcag_test_numbers_parent_id_idx" ON "wcag_test_numbers" ("_parent_id");
CREATE UNIQUE INDEX IF NOT EXISTS "wcag_clause_idx" ON "wcag" ("clause");
CREATE INDEX IF NOT EXISTS "wcag_created_at_idx" ON "wcag" ("created_at");
CREATE INDEX IF NOT EXISTS "payload_preferences_key_idx" ON "payload_preferences" ("key");
CREATE INDEX IF NOT EXISTS "payload_preferences_created_at_idx" ON "payload_preferences" ("created_at");
CREATE INDEX IF NOT EXISTS "payload_preferences_rels_order_idx" ON "payload_preferences_rels" ("order");
CREATE INDEX IF NOT EXISTS "payload_preferences_rels_parent_idx" ON "payload_preferences_rels" ("parent_id");
CREATE INDEX IF NOT EXISTS "payload_preferences_rels_path_idx" ON "payload_preferences_rels" ("path");
CREATE INDEX IF NOT EXISTS "payload_migrations_created_at_idx" ON "payload_migrations" ("created_at");
DO $$ BEGIN
 ALTER TABLE "iso_test_numbers" ADD CONSTRAINT "iso_test_numbers_parent_id_fk" FOREIGN KEY ("_parent_id") REFERENCES "iso"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
 ALTER TABLE "iso_test_numbers_locales" ADD CONSTRAINT "iso_test_numbers_locales_parent_id_fk" FOREIGN KEY ("_parent_id") REFERENCES "iso_test_numbers"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
 ALTER TABLE "wcag_test_numbers" ADD CONSTRAINT "wcag_test_numbers_parent_id_fk" FOREIGN KEY ("_parent_id") REFERENCES "wcag"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
 ALTER TABLE "wcag_test_numbers_locales" ADD CONSTRAINT "wcag_test_numbers_locales_parent_id_fk" FOREIGN KEY ("_parent_id") REFERENCES "wcag_test_numbers"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
 ALTER TABLE "payload_preferences_rels" ADD CONSTRAINT "payload_preferences_rels_parent_fk" FOREIGN KEY ("parent_id") REFERENCES "payload_preferences"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
 ALTER TABLE "payload_preferences_rels" ADD CONSTRAINT "payload_preferences_rels_users_fk" FOREIGN KEY ("users_id") REFERENCES "users"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
`);

};

export async function down({ payload }: MigrateDownArgs): Promise<void> {
await payload.db.drizzle.execute(sql`

DROP TABLE "users";
DROP TABLE "iso_test_numbers";
DROP TABLE "iso_test_numbers_locales";
DROP TABLE "iso";
DROP TABLE "wcag_test_numbers";
DROP TABLE "wcag_test_numbers_locales";
DROP TABLE "wcag";
DROP TABLE "payload_preferences";
DROP TABLE "payload_preferences_rels";
DROP TABLE "payload_migrations";`);

};
