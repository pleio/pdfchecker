const webpack = require('webpack')

module.exports = {
    plugins: [
        new webpack.DefinePlugin({
            __VUE_I18N_FULL_INSTALL__: true,
            __VUE_I18N_LEGACY_API__: false,
            __INTLIFY_PROD_DEVTOOLS__: false,
        }),
    ],
}
