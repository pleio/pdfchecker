# pdfchecker

Pdfchecker.nl frontend and backend

This application consists of:

- a Vue.js frontend
- a Payload CMS backend (see `/cms` folder)
- a VeraPDF API backend (maintained elsewhere, but deployed alongside the application)

## Project setup

`
yarn install
`

### Compiles and hot-reloads for development

`
yarn serve
`

### Compiles and minifies for production

`
yarn build
`

### Lints and fixes files

`
yarn lint
`

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Set up local API Development Environment
The test API uses basic auth which prompts for credentials when trying to submit a PDF. These credentials can be found in 1Password.

For automatic authentication, fill in the `.env` file. In the root directory, execute:

```bash
mv .env.example .env
```

If, for some reason, the test API is not available or you encounter authentication issues, there's the option to self-host the API:

```bash
git clone https://github.com/veraPDF/verapdf-webapp-server pdfchecker-api
cd pdfchecker-api
cd .docker
docker compose up
```

This should run the containers on port 8080, just like the Vue application. Modify the `vue.config.js` in the Vue application root to use the local API with the following command:

```bash
rm vue.config.js
echo "module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/' : 'http://localhost:8080/',
    devServer: {
        port: 8080,
        public: 'localhost:8080',
        proxy: {
            '^/api': {
                target: 'http://localhost',
                
            },
        },
    },
}" > vue.config.js
```

### Set up local CMS

Add
```bash
cd cms

# start the database
docker compose up database

# start the CMS
npm run dev
```

## Release guidelines

#### Development: 
Pushing to `develop` will deploy the solution to https://www.pdfchecker-test.nl

#### Production
Merging to `master` will deploy the solution to https://www.pdfchecker.nl after manual confirmation in the pipeline.

Releases are done via the Git-flow release method. This means we will first create a new release branch (branched from `develop`) with the new version number as name. For example:

```/release/1.2.0```

Update the `CHANGELOG.md` as well as the version in `package.json` within this release branch. All of the new features we are planning to release should be available on https://www.pdfchecker-test.nl to test. After everything is tested, we are ready to merge this branch into master. After merging to `master`, tag `master` with the new version (e.g. `1.1.1`). Lastly, merge the release branch into `develop` and remove the release branch. 

Step by step:

1. Create a new release branch (branched from `develop`)
   
2. Update the CHANGELOG.md
   
3. Update the `package.json` version to the same version as the release
   
4. Merge changes (step 2,3) into the release branch
   
5. Merge release branch into `master`.
   
6. Tag `master` with the new version number.
   
7. Lastly, merge the release branch into `develop` and delete the release branch.
