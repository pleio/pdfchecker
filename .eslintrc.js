module.exports = {
    parser: 'vue-eslint-parser',
    parserOptions: {
        parser: '@babel/eslint-parser',
        sourceType: 'module',
    },
    extends: ['eslint:recommended', 'plugin:prettier/recommended'],
    plugins: ['prettier'],
    rules: {
        'prettier/prettier': ['error'],
        'no-console': 'off',
    },
    env: {
        browser: true,
        node: true,
        es6: true,
    },
    globals: {
        config: 'readonly',
    },
}
