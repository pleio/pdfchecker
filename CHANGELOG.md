# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.10.0] - 18-12-2024

-   Updated packages to fix security vulnerabilities

## [1.9.0] - 15-10-2024

-   Changed only initializing the app with matomo if the site id is set
-   Fixed inconsistency issue between pdf viewer and selected rules
-   Changed only show structure errors once (iso: clause 7.4.4 -> test numbers 2 and 3)

## [1.8.1] - 23-07-2024

-   Improved gitlab pipelines

## [1.8.0] - 23-07-2024

-   Updated frontend wcag version from 2.1 to 2.2
-   Fixed layout issues on results page on mobile
-   Changed error message for TASK_PROCESSING_LIMIT_ERROR to something more appropriate
-   Added a CMS for managing the error messages and translations

## [1.7.0] - 16-05-2024

-   Changed hide errors that do not have a meta
-   Added status indicators to loading bar
-   Added translations for error messages in EN and DE languages and make them work on results page.

## [1.6.0] - 10-05-2024

-   Added second visual cue to selected bbox (pointer icon)
-   Added graceful handling of error if error meta or message is missing for a rule.
-   Added missing translation for the common.and key.
-   Changed color of warning icon in Rule to increase contrast.
-   Added FontList component for rendering fonts in Critical Errors
-   Changed hide warnings in result by default
-   Fixed language button on results page. Now it is clickable, does not have a transparent background and has a better clickable area.
-   Added url field to homepage to allow checking document through url.
-   Added toggle to show technical description of error/warning

## [1.5.1] - 17-01-2024

-   Removed Firmground logo from application

## [1.5.0] - 02-10-2023

-   Added English (Eng), German (Ger), and Dutch (Nl) language support for the entire site.
-   Dynamically retrieve tag version for both the frontend and backend on the About Me page.

## [1.4.0] - 19-10-2022

### Fixed

-   Removed logo (Internet Academy).
-   Option to show all errors when document has critical errors.
-   Made zoom buttons selectable.
-   Removed unnecessary Id's.
-   Description not readed automatically, after user focus.
-   Added titles to pages.
-   Improved contrast.
-   Break words that are to long.
-   Fixed the header structure.
-   Added descriptions to icons.
-   Hide decorative icons from screenreaders.
-   Added visual cues when selecting errors in the dropdown.

## [1.3.0] - 12-01-2022

### Added

-   A restriction on file upload size.

### Fixed

-   Fixed a routing issue where you couldn't refresh some pages or visit URL's by typing them in the address bar.
-   Fixed an issue where critical font errors and some regular font errors weren't showing the associated font.

## [1.2.0] - 22-12-2021

### Added

-   Three new partner logo's on the home screen (Firm Ground, Gehandicapten Valkenswaard, Documenten en Toegankelijkheid).

### Fixed

-   Fixed a scrolling issue when list of errors was too long.
-   Fixed some visual bugs in Safari.
-   Fixed an issue where sometimes a file upload would fail on Safari.

## [1.1.1] - 17-12-2021

### Changed

-   Errormessages.

## [1.1.0]

### Added

-   Statistics for site usage (Visitors + amount of uploaded PDFs).

### Changed

-   Errormessages.

## [1.0.2] - 03-11-2021

### Changed

-   Errormessages.

### Fixed

-   Added a fix for checks not having a referral in the document.
-   Added a fix for the visualization of the errors in the PDF.

## [1.0.1] - 20-10-2021

### Added

-   Partner logo's on the home page including links.

### Changed

-   Scoring system parameters.
-   Errormessages.
-   Support mail link now refers to the Pleio forum.
-   Favicon.
