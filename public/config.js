/* NOTE: This is a configuration for local development,
 *  its contents are overridden inside the deployment through a configmap-volume
 */

const config = (() => {
    return {
        VUE_APP_ENV_MATOMO_SITE_ID: undefined,
        VUE_APP_ENV_MATOMO_UPLOAD_GOAL_ID: undefined,
        VUE_APP_CMS_BASE_URL: 'http://localhost:8000/cms',
    }
})()
