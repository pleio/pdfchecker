# build stage
FROM node:lts-alpine as build-stage
WORKDIR /app

RUN apk update && apk add --no-cache \
    git

COPY package*.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build

# final image
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY ./docker/nginx/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 8000
CMD ["nginx", "-g", "daemon off;"]
