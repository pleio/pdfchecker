module.exports = {
    devServer: {
        proxy: {
            '^/api': {
                target: 'https://pdfchecker-test.nl',
                changeOrigin: true,
                auth: `${process.env.VUE_APP_USERNAME}:${process.env.VUE_APP_PASSWORD}`,
            },
        },
    },
}
